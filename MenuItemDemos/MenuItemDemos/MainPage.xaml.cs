﻿using Xamarin.Forms;

namespace MenuItemDemos
{
	public partial class MainPage : TabbedPage
	{
		public MainPage()
		{
			InitializeComponent();
			//Following we have defined 2 tabs where we are setting Icon image here. Every player has their own unique Icon
			var BasketBall = new NavigationPage(new BasketBall());
			var Soccer = new NavigationPage(new Soccer());
			


			BasketBall.Title = "BasketBall";
			Soccer.Title = "Soccer";
			
			

			Children.Add(BasketBall);
			
			Children.Add(Soccer);
			

		}
	}
}
