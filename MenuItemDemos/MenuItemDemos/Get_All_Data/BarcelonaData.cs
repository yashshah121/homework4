﻿using MenuItemDemos.Mode;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MenuItemDemos
{
    public static class BarcelonaData
    {
        //Setting all the data
        public static IList<Player> Players { get; set; }
        static BarcelonaData()
        {
            Players = new ObservableCollection<Player>() {
                new Player
                {
                    Name = "Lionel Messi",
                    Position = "Forward",
                    Team = "Barcelona",
                    Url = "https://talksport.com/wp-content/uploads/sites/5/2019/08/NINTCHDBPICT000511086432-e1565876229448.jpg?strip=all&w=960&quality=100"
                },
                new Player
                {
                    Name = "Luis Suárez",
                    Position = "Forward",
                    Team = "Barcelona",
                    Url = "https://specials-images.forbesimg.com/imageserve/1192763974/960x0.jpg?fit=scale"
                },
                new Player
                {
                    Name = "Antoine Griezmann",
                    Position = "Mid Fielder",
                    Team = "Forward",
                    Url = "https://specials-images.forbesimg.com/imageserve/1204607811/960x0.jpg?fit=scale"
                },
                new Player
                {
                    Name = "Ousmane Dembélé",
                    Position = "Forward",
                    Team = "Barcelona",
                    Url = "https://as01.epimg.net/futbol/imagenes/2020/03/03/primera/1583217771_568161_1583218460_noticia_normal_recorte1.jpg"
                },
                new Player
                {
                    Name = "Ansu Fati",
                    Position = "Forward",
                    Team = "Barcelona",
                    Url = "https://e00-marca.uecdn.es/assets/multimedia/imagenes/2019/09/21/15690757884806.jpg"
                },
                new Player
                {
                    Name = "Frenkie de Jong",
                    Position = "Mid Fielder",
                    Team = "Barcelona",
                    Url = "https://en.as.com/en/imagenes/2019/12/29/football/1577607349_992427_noticia_normal.jpg"
                },
                new Player
                {
                    Name = "Arturo Vidal",
                    Position = "Mid Fielder",
                    Team = "Barcelona",
                    Url = "https://img.bleacherreport.net/img/images/photos/003/846/372/hi-res-e15800021748649cf488e06189588370_crop_north.jpg?h=533&w=800&q=70&crop_x=center&crop_y=top"
                },
                new Player
                {
                    Name = "Arthur Melo",
                    Position = "Mid Fielder",
                    Team = "Barcelona",
                    Url = "https://www.fcbarcelonanoticias.com/uploads/s1/11/83/21/8/arthur-melo-sigue-sin-jugar.jpeg"
                },
                new Player
                {
                    Name = "Gerard Piqué",
                    Position = "Defender",
                    Team = "Barcelona",
                    Url = "https://upload.wikimedia.org/wikipedia/commons/f/fa/Gerard_Piqu%C3%A9_2017.jpg"
                },
                    new Player
                {
                    Name = "Marc-André ter Stegen",
                    Position = "Goal Keeper",
                    Team = "Barcelona",
                    Url = "https://gmsrp.cachefly.net/images/20/03/04/9a479c9b2f9cd6eb16fb5b9c47a50ac3/320.jpg"
                    },
                    new Player
                {
                    Name = "Ivan Rakitić",
                    Position = "Mid Fielder",
                    Team = "Barcelona",
                    Url = "https://tmssl.akamaized.net//images/foto/normal/ivan-rakitic-vom-fc-barcelona-1533818758-17079.jpg"

                },



            };
        }
    }
}
