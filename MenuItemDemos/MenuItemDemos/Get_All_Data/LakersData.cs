﻿using MenuItemDemos.Mode;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MenuItemDemos
{
    public static class LakersData
    {
        public static IList<Player> Players { get; set; }
        static LakersData()
        {
            Players = new ObservableCollection<Player>() {
                 new Player
                {
                     //Setting all the data
                    Name = "Kobe Bryant",
                    Position = "Shooting guard",
                    Team = "Los Angeles Lakers",
                    Url = "https://mir-s3-cdn-cf.behance.net/project_modules/disp/f3121a7457241.560abb7651116.png"
                },
                new Player
                {
                    Name = "LeBron James",
                    Position = "Power forward",
                    Team = "Cleveland Cavaliers",
                    Url="https://www.freeiconspng.com/uploads/lebron-james-png-25.png"

                },
                new Player{
                    Name ="Anthony Davis",

                    Position = "Power Forward",
                    Team ="Lakers",
                    Url ="https://cdn.shopify.com/s/files/1/2030/1917/products/ADICPV.jpg?v=1576198640"
                },
                new Player
                {
                    Name ="Alex Caruso",
                    Position = "Shooting Gaurd",
                    Team = "Los Angeles Lakers",
                    Url = "https://i.insider.com/5d7262c52e22af1add2984e6?width=1100&format=jpeg&auto=webp"
                },

                new Player
                {
                    Name = "Dwight Howard",
                    Position = "Shooting guard",
                    Team = "New Yersey nets",
                    Url = "https://s.yimg.com/ny/api/res/1.2/XY9uAJp2SPYGpG7zNxBByA--~A/YXBwaWQ9aGlnaGxhbmRlcjtzbT0xO3c9ODAw/https://media-mbst-pub-ue1.s3.amazonaws.com/creatr-images/2020-01/c7952fe0-30ad-11ea-8c5f-a0f983297f72"

                },
            };
        }
    }
}
