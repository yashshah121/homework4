﻿using MenuItemDemos.Mode;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MenuItemDemos
{
    public static class ChicagoData
    {
        public static IList<Player> Players { get; set; }
        static ChicagoData()
        {
            //Setting all the data
            Players = new ObservableCollection<Player>() {
                new Player
                {
                    Name = "Zach LaVine",
                    Position = "Small Forward",
                    Team = "Chicago Bulls",
                    Url = "https://c.static-nike.com/a/images/t_PDP_1280_v1/f_auto/xzlm6qaxbphdqk966lbr/zach-lavine-bulls-icon-edition-nba-swingman-jersey-lPFCqf.jpg"
                },
                new Player
                {
                    Name = "Coby WHite",
                    Position = "Point Guard",
                    Team = "Chicago Bulls",
                    Url="https://img.bleacherreport.net/img/images/photos/003/855/864/hi-res-8cc990e57bc6198e1b465f81e6f44665_crop_north.jpg?h=533&w=800&q=70&crop_x=center&crop_y=top"

                },
                new Player{
                    Name ="Lauri Markkanen",

                    Position = "Power Forward",
                    Team ="Chicago Bulls",
                    Url ="https://www.nbcsports.com/chicago/sites/csnchicago/files/2020/03/04/lauriherooo.jpg"
                },
                new Player
                {
                    Name ="Otto Porter Jr.",
                    Position = "Power Forward",
                    Team = "Chicago Bulls",
                    Url = "https://img.bleacherreport.net/img/images/photos/003/844/560/hi-res-c41f5834ccf8922c201869bb27eb2b57_crop_north.jpg?h=533&w=800&q=70&crop_x=center&crop_y=top"
                },

                new Player
                {
                    Name = "Wendell Carter Jr.",
                    Position = "Center",
                    Team = "Chicago Bulls",
                    Url = "https://theundefeated.com/wp-content/uploads/2017/04/wendellcarterjr-0461.jpg?w=1500"

                },
            };
        }
    }
}
