﻿using MenuItemDemos.Mode;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MenuItemDemos
{
    public static class GoldenData
    {
        public static IList<Player> Players { get; set; }
        static GoldenData()
        {
            //Setting all the data
            Players = new ObservableCollection<Player>() {
                new Player
                {
                    Name = "Stephen curry",
                    Position = "Point guard",
                    Team = "Golden State Warriors",
                    Url = "https://specials-images.forbesimg.com/imageserve/1179505916/960x0.jpg?fit=scale"
                },
                new Player
                {
                    Name = "Klay Thompson",
                    Position = "Small forward",
                    Team = "Golden State Warriors",
                    Url="https://specials-images.forbesimg.com/imageserve/1155800281/960x0.jpg?fit=scale"

                },
                new Player{
                    Name ="Draymond Green",

                    Position = "Power Forward",
                    Team ="Chicago Bulls",
                    Url ="https://a4.espncdn.com/combiner/i?img=%2Fi%2Fheadshots%2Fnba%2Fplayers%2Ffull%2F6589.png"
                },
                new Player
                {
                    Name ="Andrew Wiggins",
                    Position = "Small Forward",
                    Team = "Golden State Warriors",
                    Url = "https://www.si.com/.image/t_share/MTY4NjIzOTY5NTg5NzMyOTk5/andrew-wiggins-timberwolves-lead.jpg"
                },

                new Player
                {
                    Name = "Eric Paschall",
                    Position = "Power Forward",
                    Team = "New Yersey nets",
                    Url = "https://upload.wikimedia.org/wikipedia/commons/f/fd/20170213_Villanova-Depaul_Eric_Paschall_dribbling.jpg"

                },

            };
        }
    }
}
