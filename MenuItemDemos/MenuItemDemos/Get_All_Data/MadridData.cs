﻿using MenuItemDemos.Mode;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MenuItemDemos
{
    public static class MadridData
    {
        public static IList<Player> Players { get; set; }
        static MadridData()
        {
            //Setting all the data
            Players = new ObservableCollection<Player>() {
                new Player
                {
                    Name = "Vinícius Júnior",
                    Position = "Forward",
                    Team = "Madrid",
                    Url = "https://images2.minutemediacdn.com/image/fetch/w_736,h_485,c_fill,g_auto,f_auto/https%3A%2F%2Ftherealchamps.com%2Fwp-content%2Fuploads%2Fgetty-images%2F2017%2F07%2F1209900055-850x560.jpeg"
                },
                new Player
                {
                    Name = "Mariano Diaz",
                    Position = "Forward",
                    Team = "Madrid",
                    Url="https://en.as.com/futbol/imagenes/2019/11/22/primera/1574458222_084261_1574458462_noticia_normal.jpg"

                },
                new Player{
                    Name ="Eden Hazard",

                    Position = "Forward",
                    Team ="Madrid",
                    Url ="https://talksport.com/wp-content/uploads/sites/5/2019/09/NINTCHDBPICT000523052102-e1576354873957.jpg?strip=all&w=960&quality=100"
                },
                new Player
                {
                    Name ="Karim Benzema",
                    Position = "Forward",
                    Team = "Madrid",
                    Url = "https://as01.epimg.net/futbol/imagenes/2019/11/26/primera/1574754197_944965_1574755303_noticia_normal_recorte1.jpg"
                },

                new Player
                {
                    Name = "Gareth Bale",
                    Position = "Forward",
                    Team = "Madrid",
                    Url = "https://specials-images.forbesimg.com/imageserve/1140814908/960x0.jpg?cropX1=506&cropX2=4457&cropY1=0&cropY2=2562"

                },
                new Player
                {
                    Name = "Federico Valverde",
                    Position = "MidFielder",
                    Team = "Madrid",
                    Url = "https://tmssl.akamaized.net//images/foto/normal/federico-valverde-real-madrid-1579707325-30263.jpg"

                },
                new Player
                {
                    Name = "Sergio Ramos",
                    Position = "Defender",
                    Team = "Madrid",
                    Url = "https://en.as.com/tikitakas/imagenes/2019/02/23/portada/1550919555_521513_1550919850_noticia_normal.jpg"

                },
                new Player
                {
                    Name = "James Rodríguez",
                    Position = "MidFielder",
                    Team = "Madrid",
                    Url = "https://upload.wikimedia.org/wikipedia/commons/7/79/James_Rodriguez_2018.jpg"

                },
                new Player
                {
                    Name = "Rodrygo",
                    Position = "Forward",
                    Team = "Madrid",
                    Url = "https://specials-images.forbesimg.com/imageserve/1180472815/960x0.jpg?fit=scale"

                },
                new Player
                {
                    Name = "Marcelo Vieira",
                    Position = "Defender",
                    Team = "Madrid",
                    Url = "https://allstarbio.com/wp-content/uploads/2018/06/Marcelo-VieiraBio-Net-worth-Height-Body-Girlfriend-Affair-Married-Ethnicity.jpg"

                },
                new Player
                {
                    Name = "Luka Modrić",
                    Position = "MidFielder",
                    Team = "Madrid",
                    Url = "https://tmssl.akamaized.net//images/foto/normal/luka-modric-real-madrid-1571213383-26597.jpg"

                },
            };
        }
    }
}
