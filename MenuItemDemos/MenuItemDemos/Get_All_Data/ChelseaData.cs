﻿using MenuItemDemos.Mode;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MenuItemDemos
{
    public static class ChelseaData
    {
        //Setting all the data
        public static IList<Player> Players { get; set; }
        static ChelseaData(){

            
            Players = new ObservableCollection<Player>() {
               new Player
                {
                    Name = "Billy Gilmour",
                    Position = "Mid Fielder",
                    Team = "Chelsea",
                    Url = "https://i.dailymail.co.uk/1s/2020/03/04/12/25525990-0-image-a-43_1583324904028.jpg"
                },
               new Player
                {
                    Name = "Christian Pulisic",
                    Position = "Mid Fielder",
                    Team = "Chelsea",
                    Url = "https://fivethirtyeight.com/wp-content/uploads/2019/11/GettyImages-1185083127-1-e1573173671498.jpg?w=575"
                },
               new Player
                {
                    Name = "Kepa Arrizabalaga",
                    Position = "Goal keeper",
                    Team = "Chelsea",
                    Url = "https://images.cdn.fourfourtwo.com/sites/fourfourtwo.com/files/styles/image_landscape/public/2.48634531.jpg?itok=tgOXIRO2&c=87b6d99828d88c1b8ffe17a08d24fc7d"
                },
               new Player
                {
                    Name = "Willian",
                    Position = "Forward",
                    Team = "Chelsea",
                    Url = "https://cdn.images.express.co.uk/img/dynamic/67/590x/Chelsea-news-Willian-1251110.jpg?r=1583389747321"
                },
               new Player
                {
                    Name = "Tammy Abraham",
                    Position = "Forward",
                    Team = "Chelsea",
                    Url = "https://e0.365dm.com/19/12/768x432/skysports-tammy-abraham-chelsea_4862633.jpg?20191210202927"
                },
               new Player
                {
                    Name = "Olivier Giroud",
                    Position = "Forward",
                    Team = "Chelsea",
                    Url = "https://i.dailymail.co.uk/1s/2020/02/01/12/24167990-0-image-a-6_1580561798757.jpg"
                },
               new Player
                {
                    Name = "N'Golo Kanté",
                    Position = "MidFielder",
                    Team = "Chelsea",
                    Url = "https://resources.premierleague.com/premierleague/photos/players/250x250/p116594.png"
                },
               new Player
                {
                    Name = "Pedro",
                    Position = "Forward",
                    Team = "Chelsea",
                    Url = "https://cdn.newsserve.net/700/en/knowledge/Pedro-footballer-born-_20190502.jpg"
                },
               new Player
                {
                    Name = "Marcos Alonso",
                    Position = "Defender",
                    Team = "Chelsea",
                    Url = "https://tmssl.akamaized.net//images/foto/normal/marcos-alonso-1489045832-8848.jpg"
                },
               new Player
                {
                    Name = "Mason Mount",
                    Position = "Small Forward",
                    Team = "MidFielder",
                    Url = "https://resources.premierleague.com/premierleague/photos/players/250x250/p184341.png"
                },
               new Player
                {
                    Name = "Jorginho",
                    Position = "Midfielder",
                    Team = "Chelsea",
                    Url = "https://talksport.com/wp-content/uploads/sites/5/2019/02/GettyImages-1088010174.jpg?strip=all&w=960&quality=100"
                },
            };
        }
    }
}
