﻿using MenuItemDemos.Mode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MenuItemDemos
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PlayerDetail : ContentPage
    {
        public PlayerDetail(Player player)
        {
            InitializeComponent();
            Name.Text = player.Name;
            Team.Text = player.Team;
            Position.Text = player.Position;
        }
    }
}