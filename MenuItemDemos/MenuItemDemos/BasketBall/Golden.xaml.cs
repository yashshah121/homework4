﻿using MenuItemDemos.Mode;

using System;
using System.Collections;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace MenuItemDemos
{
    public partial class Golden : ContentPage
    {
        private IEnumerable _save = GoldenData.Players;
        [Obsolete]
        public Golden()
        {
            //getting data from the data file located in the differen folder

            InitializeComponent();
            playerListView.ItemsSource =GoldenData.Players;
            playerListView.RefreshCommand = new Command(() => {
                //Do your stuff.
                RefreshData();
                playerListView.IsRefreshing = false;
            });

        }
        public void RefreshData()
        {
            //wehn pulled down this will be called and everything will be resest to the orginal view.
            GoldenData.Players = new ObservableCollection<Player>() {
               new Player
                {
                    Name = "Stephen curry",
                    Position = "Point guard",
                    Team = "Golden State Warriors",
                    Url = "https://specials-images.forbesimg.com/imageserve/1179505916/960x0.jpg?fit=scale"
                },
                new Player
                {
                    Name = "Klay Thompson",
                    Position = "Small forward",
                    Team = "Golden State Warriors",
                    Url="https://specials-images.forbesimg.com/imageserve/1155800281/960x0.jpg?fit=scale"

                },
                new Player{
                    Name ="Draymond Green",

                    Position = "Power Forward",
                    Team ="Chicago Bulls",
                    Url ="https://a4.espncdn.com/combiner/i?img=%2Fi%2Fheadshots%2Fnba%2Fplayers%2Ffull%2F6589.png"
                },
                new Player
                {
                    Name ="Andrew Wiggins",
                    Position = "Small Forward",
                    Team = "Golden State Warriors",
                    Url = "https://www.si.com/.image/t_share/MTY4NjIzOTY5NTg5NzMyOTk5/andrew-wiggins-timberwolves-lead.jpg"
                },

                new Player
                {
                    Name = "Eric Paschall",
                    Position = "Power Forward",
                    Team = "New Yersey nets",
                    Url = "https://upload.wikimedia.org/wikipedia/commons/f/fd/20170213_Villanova-Depaul_Eric_Paschall_dribbling.jpg"

                },

            };
            playerListView.ItemsSource = null;
            playerListView.ItemsSource = GoldenData.Players;


        }
        private async void OnItemTapped(object sender, ItemTappedEventArgs e)
        {

            //on tap will take to new page which will give us detail of that particular page sent using object.
            var item = e.Item as Player;

            await Navigation.PushAsync(new PlayerDetail(item));
        }

        private void OnDelete(object sender, System.EventArgs e)
        {
            //when conetxt menu choosen it will delete using this code
            var listItem = ((MenuItem)sender);
            var player = (Player)listItem.CommandParameter;

            GoldenData.Players.Remove(player);
        }

    }
}