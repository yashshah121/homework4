﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MenuItemDemos
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BasketBall : ContentPage
    {
        //Used Table here which shiws 3 teams on clicking we will be redirected to the respetive page of the team
        public ICommand NavigateCommand { get; set; }
        public BasketBall()
        {
            InitializeComponent();
            NavigateCommand = new Command<Type>(async (Type pageType) =>
            {
                Page page = (Page)Activator.CreateInstance(pageType);
                await Navigation.PushAsync(page);
            });

            BindingContext = this;
        }
    }
}