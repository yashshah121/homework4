﻿using MenuItemDemos.Mode;

using System;
using System.Collections;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace MenuItemDemos
{
    public partial class Lakers : ContentPage
    {
        private IEnumerable _save = LakersData.Players;
        [Obsolete]
        public Lakers()
        {
            //getting data from the data file located in the differen folder

            InitializeComponent();
            playerListView.ItemsSource = LakersData.Players;
            playerListView.RefreshCommand = new Command(() => {
                //Do your stuff.
                RefreshData();
                playerListView.IsRefreshing = false;
            });

        }
        public void RefreshData()
        {
            //wehn pulled down this will be called and everything will be resest to the orginal view.
            LakersData.Players = new ObservableCollection<Player>() {
                new Player
                {
                    Name = "Kobe Bryant",
                    Position = "Shooting guard",
                    Team = "Los Angeles Lakers",
                    Url = "https://mir-s3-cdn-cf.behance.net/project_modules/disp/f3121a7457241.560abb7651116.png"
                },
                new Player
                {
                    Name = "LeBron James",
                    Position = "Power forward",
                    Team = "Cleveland Cavaliers",
                    Url="https://www.freeiconspng.com/uploads/lebron-james-png-25.png"

                },
                new Player{
                    Name ="Anthony Davis",

                    Position = "Power Forward",
                    Team ="Lakers",
                    Url ="https://cdn.shopify.com/s/files/1/2030/1917/products/ADICPV.jpg?v=1576198640"
                },
                new Player
                {
                    Name ="Alex Caruso",
                    Position = "Shooting Gaurd",
                    Team = "Los Angeles Lakers",
                    Url = "https://i.insider.com/5d7262c52e22af1add2984e6?width=1100&format=jpeg&auto=webp"
                },

                new Player
                {
                    Name = "Dwight Howard",
                    Position = "Shooting guard",
                    Team = "New Yersey nets",
                    Url = "https://s.yimg.com/ny/api/res/1.2/XY9uAJp2SPYGpG7zNxBByA--~A/YXBwaWQ9aGlnaGxhbmRlcjtzbT0xO3c9ODAw/https://media-mbst-pub-ue1.s3.amazonaws.com/creatr-images/2020-01/c7952fe0-30ad-11ea-8c5f-a0f983297f72"

                },
            };
            playerListView.ItemsSource = null;
            playerListView.ItemsSource = LakersData.Players;


        }
        private async void OnItemTapped(object sender, ItemTappedEventArgs e)
        {

            //on tap will take to new page which will give us detail of that particular page sent using object.
            var item = e.Item as Player;

            await Navigation.PushAsync(new PlayerDetail(item));
        }

        private void OnDelete(object sender, System.EventArgs e)
        {
            //when conetxt menu choosen it will delete using this code
            var listItem = ((MenuItem)sender);
            var player = (Player)listItem.CommandParameter;

            LakersData.Players.Remove(player);
        }
    }
}